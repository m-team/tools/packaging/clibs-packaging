
Name: clibs-list
Version: 0.4.0
Release: 1%{?dist}

Summary: C doubly linked list implementation

%if 0%{?suse_version} > 0
Group: Misc
%endif


License: MIT
URL: https://github.com/clibs/list
Source0: https://github.com/clibs/list/archive/refs/heads/master.zip
Patch0: https://codebase.helmholtz.cloud/m-team/tools/packaging/clibs-packaging/-/raw/main/list/build-debuginfo.patch
BuildRequires: gcc >= 4.8


%description
C doubly linked list implementation.


%prep
#%setup -n list-{version} -q
%setup -n list-master -q
ls -la
%patch0 -p 1

%build
make 

%install
make install PREFIX=%{buildroot}\
    LIBDIR=%{buildroot}%{_libdir}
mkdir -p %{buildroot}/%{_defaultdocdir}/%{name}
ls -la
cp Readme.md %{buildroot}/%{_defaultdocdir}/%{name}/Readme.md
mkdir -p %{buildroot}/usr/share/licenses/%{name}
cp LICENSE %{buildroot}/usr/share/licenses/%{name}


#%files -n liboidc-agent4
%files
%license LICENSE
%{_libdir}/liblist.so.%{version}
%exclude %attr(0644, root, root) %{_libdir}/liblist.a
# Strange that this one was actually included:
#%exclude /usr/lib/.build-id/44/*

%changelog
* Wed Jul 27 2022 Marcus Hardt <hardt@kit.edu> - 0.2.0-1
- Initial packaging
