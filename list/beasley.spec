Name:           clibs-list
Version:        0.4.0
%global         so_version 0
Release:        %autorelease
Summary:        C doubly linked list implementation

License:        MIT
URL:            https://github.com/clibs/list
Source0:        https://github.com/clibs/list/archive/refs/tags/%{version}.tar.gz

# Backport PR#43 "Minor fixes for debian packagin", not yet merged upstream:
# Fix sonames
# Fix Version 
# Fix lib*so* symlinks
#
# https://github.com/clibs/list/pull/43
Patch:          %{url}/pull/43.patch

BuildRequires:  gcc
BuildRequires:  make

%global common_description %{expand:
%{summary}.}

%description %{common_description}

The clibs-list package contains the clibs/list library.


%package devel
Summary:        Development files for clibs-list

Requires:       %{name}%{?_isa} = %{version}-%{release}

%description devel %{common_description}

The clibs-list-devel package contains libraries and header files for developing
applications that use clibs-list.


%prep
%autosetup -n list-%{version} -p1


%build
%set_build_flags
%make_build \
    AR="${AR-gcc-ar}" CC="${CC-gcc}" STRIP=/bin/true \
    CFLAGS="${CFLAGS}" LDFLAGS="${LDFLAGS}" \
    all bin/test


%install
# Install header to a subdirectory because the name “list.h” is overly generic.
#
# Install with less generic names?
# https://github.com/clibs/list/issues/38
%make_install PREFIX='%{_prefix}' LIBDIR='%{_libdir}' INCLUDEDIR='%{_includedir}'
# We did not want the static library.
rm -vf '%{buildroot}%{_libdir}/liblist.a'


%check
%make_build test


%files
%license LICENSE
%{_libdir}/libclibs_list.so.%{so_version}{,.*}


%files devel
%doc History.md
%doc Readme.md
# This directory should be co-owned with anything else from
# https://github.com/clibs/, e.g. https://github.com/clibs/buffer/, if packaged
# in the future:
%dir %{_includedir}/clibs
%{_includedir}/clibs/list.h
%{_libdir}/libclibs_list.so
%{_libdir}/libclibs_list.a


%changelog

* Thu Jan 05 2023 Marcus Hardt marcus@hardt-it.de 0.4.0
- Adapt specfile from Ben Beasley
- Bump Version to 0.4.0
